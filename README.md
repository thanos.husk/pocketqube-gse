# PocketQube Ground Support Equipment

## Description
This repo contains everything associated with the PocketQube GSE designed and used by Libre Space Foundation to test our PocketQube satellites.    
For these tests, two different MGSEs were developed. 
1. Spring rail GSE   
This GSE is used to simulate the conditions inside the PICOBUS deployer. The same linear spring is used to hold the PocketQubes on place.

2. Clamp rail GSE   
This GSE is used to firmly clamp the PocketQubes in place, allowing no movement during tests. The holding area is the same as the one inside the PICOBUS deployer. 

## License
This project is licensed under the [CERN Open Hardware Licence v1.2](https://gitlab.com/thanos.husk/pocketqube-gse/-/blob/main/%EF%BB%BFCERN%20Open%20Hardware%20Licence%20v1.2%20?ref_type=heads)

##
